package ru.t1.rydlev.tm.exception.field;

public class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! UserId is empty...");
    }

}
