package ru.t1.rydlev.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Oleg Rydlev");
        System.out.println("E-MAIL: work.akrr@gmail.com");
    }

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getName() {
        return "info";
    }

}
