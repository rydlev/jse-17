package ru.t1.rydlev.tm.command.user;

import ru.t1.rydlev.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getDescription() {
        return "Logout current user.";
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
